@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New To Do Item
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Task Form -->
                    <form action="{{ url('task')}}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <!-- Task Name -->
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Task</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="task-name" class="form-control" value="{{ old('task') }}">
                            </div>
                            
                    
                        </div>
                        
                        <div class="form-group">

                         <!-- Priority -->
                            <div class="col-sm-offset-3 col-sm-6">
                                 <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Priority
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li onclick="$('#task-priority').val('1');"><a href="#">1</a></li>
                                  <li onclick="$('#task-priority').val('2');"><a href="#">2</a></li>
                                  <li onclick="$('#task-priority').val('3');"><a href="#">3</a></li>
                                </ul>
                                 <input type="text" name="priority" id="task-priority" class="form-control" value="1">
                              </div>
                              </div>
                          
                            </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-btn fa-plus"></i>Add Task
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Current Tasks -->
            @if (count($tasks) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Tasks
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped task-table">
                            <thead>
                                <th>Task Item</th>
                                 <th>Priority</th>
                                <th>&nbsp;</th>
                            </thead>
                            <tbody>
                                @foreach ($tasks as $task)
                                    <tr>
                                        <td class="table-text"><div>{{ $task->name }}</div></td>
                                        <td class="table-text"><div>{{ $task->priority }}</div></td>

                                        <!-- Task Delete Button -->
                                        <td>
                                            <!-- spoof delete method with method_field('DELETE')  which outputs <input type="hidden" name="_method" value="DELETE"> -->
                                            <form action="{{ url('task/'.$task->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            @endif
            
           <div class="panel panel-default">
                    <div class="panel-heading">
                        Priority Percentage
                    </div>                    
                    <div class="panel-body">
            <script>

            var chart;
            var legend;

            var chartData = [{
                priority: "P1",
                sumpriority: {{ $chart_data[0]->priority }}
            }, {
                priority: "P2",
                sumpriority: {{ $chart_data[1]->priority }}
            }, {
                priority: "P3",
                sumpriority: {{ $chart_data[2]->priority }}
            }];


            AmCharts.ready(function () {
                // PIE CHART
                chart = new AmCharts.AmPieChart();
                chart.dataProvider = chartData;
                chart.titleField = "priority";
                chart.valueField = "sumpriority";
                chart.outlineColor = "#FFFFFF";
                chart.outlineAlpha = 0.8;
                chart.outlineThickness = 2;

                // WRITE
                chart.write("chartdiv");
            });

            </script>
                        <div id="chartdiv" style="width: 100%; height: 400px;"></div>
                    </div>
            </div>

            
        </div>
    </div>
@endsection