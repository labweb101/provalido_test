<?php

use App\Task;
use Illuminate\Http\Request;

/**
 * Show Task Dashboard
 */
Route::get('/', function () {
    
    //return tasks
    $tasks = Task::orderBy('created_at', 'asc')->get();
    
    //get data for chart
    $chart_data = DB::table("tasks")
	        ->select(
	            'priority'
	            ,DB::raw("SUM(1) as total_priority"))
	        ->groupBy('priority')
	        ->get();

    return view('tasks', [
        'tasks' => $tasks,
        'chart_data' => $chart_data
    ]);
   
    
});

/**
 * Add New Task
 */
Route::post('task', function (Request $request) {

    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
        'priority' => 'required|max:255'
    ]);

    if ($validator->fails()) {
        return redirect('/')
            ->withInput()
            ->withErrors($validator);
    }

    // Create The Task...
    $task = new Task;
    $task->name = $request->name;
    $task->priority = $request->priority;
    $task->save();

    return redirect('/');

});

/**
 * Delete Task
 */

Route::delete('task/{task}', function (Task $task) {

    $task->delete();

    return redirect('/');
});